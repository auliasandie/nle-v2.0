import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Portlet,
  PortletBody
} from "../partials/content/Portlet";

export default function Documents() {
  const [documents] = useState([{
    type: 'PEB',
    id: '987065',
    date: '2 August 2019',
    status: 'NPE - Ready to Export',
    progress: 25
  }, {
    type: 'PEB',
    id: '758954',
    date: '2 August 2019',
    status: 'NPE - Ready to Export',
    progress: 75
  }, {
    type: 'PIB',
    id: '124329',
    date: '17 September 2019',
    status: 'SPPB - Release',
    progress: 50
  }, {
    type: 'PIB',
    id: '875331',
    date: '20 September 2019',
    status: 'SPPB - Release',
    progress: 100
  }, {
    type: 'PEB',
    id: '678502',
    date: '23 September 2019',
    status: 'NPE - Ready to Export',
    progress: 100
  }]);

  const [details] = useState([{
    product: {
      hsCode: '0306.16.22',
      desc: 'Udang Vaname',
      alias: 'Liptopenaeus Vannamei',
      merk: 'BabaMirza',
      price: 'IDR 83.000',
      tax: '10%',
      unit: '200 Box',
      netto: '10000 KGM',
    },
    documents: [{
      type: 'Quarantine',
      num: '3212/KP/2019',
      date: '17 Aug 2019'
    }, {
      type: 'COO',
      num: '3212/KP/2019',
      date: '27 Aug 2019'
    }],
    fob: 'IDR 830.000.000'
  }, {
    product: {
      hsCode: '0301.94.00',
      desc: 'Ikan Tuna',
      alias: 'Thunnus Orientalis',
      merk: 'SamAmir',
      price: 'IDR 48.000',
      tax: '0%',
      unit: '500 Box',
      netto: '20000 KGM',
    },
    documents: [{
      type: 'Quarantine',
      num: '3212/KP/2019',
      date: '17 Aug 2019'
    }, {
      type: 'COO',
      num: '3212/KP/2019',
      date: '27 Aug 2019'
    }],
    fob: 'IDR 960.000.000'
  }, {
    product: {
      hsCode: '0302.89.18',
      desc: 'Ikan Kakap Merah',
      alias: 'Lutjanus Argentimaculatus',
      merk: 'BossPur',
      price: 'IDR 60.000',
      tax: '0%',
      unit: '300 Box',
      netto: '15000 KGM',
    },
    documents: [{
      type: 'Quarantine',
      num: '3212/KP/2019',
      date: '17 Aug 2019'
    }, {
      type: 'COO',
      num: '3212/KP/2019',
      date: '27 Aug 2019'
    }],
    fob: 'IDR 900.000.000'
  }, {
    product: {
      hsCode: '0306.33.00',
      desc: 'Kepiting',
      alias: 'Liocarcinus Depurator',
      merk: 'CakDeny',
      price: 'IDR 85.000',
      tax: '0%',
      unit: '100 Box',
      netto: '10000 KGM',
    },
    documents: [{
      type: 'Quarantine',
      num: '3212/KP/2019',
      date: '17 Aug 2019'
    }, {
      type: 'COO',
      num: '3212/KP/2019',
      date: '27 Aug 2019'
    }],
    fob: 'IDR 850.000.000'
  }])

  const [tab, setTab] = useState('tracking');

  const handleProgressBg = (value) => {
    if (value > 75) {
      return "success"
    } else if (value > 50) {
      return "info"
    } else if (value > 25) {
      return "warning"
    } else {
      return "danger"
    }
  }

  return (
    <>
      <div id="kt_subheader" className="kt-portlet kt-portlet--height-fluid">
        <div className="kt-portlet__head">
          <div className="kt-portlet__head-label">
            <h3 className="kt-portlet__head-title">Documents</h3>
          </div>
          <div className="kt-portlet__head-toolbar">
            <Link to="/documents/add" className="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
              Add New Document
            </Link>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletBody fluid={true} className="px-0">
              <div className="table-responsive">
                <table className="table table-md">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Document Type</th>
                      <th>Document ID</th>
                      <th>Date</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {documents.map((item, index) => {
                      return (
                        <tr key={index}>
                          <th scope="row">
                            <label className={'kt-checkbox kt-checkbox--solid kt-checkbox--' + handleProgressBg(item.progress)}>
                              <input type="checkbox" disabled/>
                              <span></span>
                            </label>
                          </th>
                          <td>{item.type}</td>
                          <td>{item.id}</td>
                          <td>{item.date}</td>
                          <td>{item.status}</td>
                          <td className="text-right">
                            <button className="btn-label-info btn btn-sm btn-bold btn-action">Details</button>
                            &nbsp;&nbsp;&nbsp;
                            <button className="btn-label-success btn btn-sm btn-bold btn-action">Print</button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
            </PortletBody>
          </Portlet>
        </div>
        <div className="col-12">
          <div className="kt-portlet kt-portlet--tabs">
            <div className="kt-portlet__head">
              <div className="kt-portlet__head-toolbar">
                <ul className="nav nav-tabs nav-tabs-line nav-tabs-line-danger nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
                  <li className="nav-item">
                    <a className={'nav-link ' + (tab === 'tracking' ? 'active' : '')} data-toggle="tab" role="tab" onClick={() => setTab('tracking')}>
                      <i className="flaticon2-location" aria-hidden="true"></i>Tracking
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className={'nav-link ' + (tab === 'pib-header' ? 'active' : '')} data-toggle="tab" role="tab" onClick={() => setTab('pib-header')}>
                      <i className="flaticon2-list-2" aria-hidden="true"></i>PIB Header
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className={'nav-link ' + (tab === 'pib-details' ? 'active' : '')} data-toggle="tab" role="tab" onClick={() => setTab('pib-details')}>
                      <i className="flaticon2-list-3" aria-hidden="true"></i>PIB Details
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className={'nav-link ' + (tab === 'billing' ? 'active' : '')} data-toggle="tab" role="tab" onClick={() => setTab('billing')}>
                      <i className="flaticon2-percentage" aria-hidden="true"></i>Billing
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className={'nav-link ' + (tab === 'shipments-details' ? 'active' : '')} data-toggle="tab" role="tab" onClick={() => setTab('shipments-details')}>
                      <i className="flaticon2-delivery-package" aria-hidden="true"></i>Shipments Details
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className={'nav-link ' + (tab === 'trucking-details' ? 'active' : '')} data-toggle="tab" role="tab" onClick={() => setTab('trucking-details')}>
                      <i className="flaticon2-delivery-truck" aria-hidden="true"></i>Trucking Details
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="kt-portlet__body p-0">
              <div className="tab-content">
                {tab === 'tracking' ? (
                  <div className="tab-pane active p-4" role="tabpanel">
                    <div className="">
                      <p><b>BL Number : SN123ASDA123</b></p>
                      <br />
                      <ul className="list-ic horizontal">
                        <li>
                          <img className="img-fluid" src={'/media/ceisa/document-1.jpg'} alt={'Document 1'} />
                          <ul className="list-ic vertical">
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">Request DO</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">DO Payment</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">Do Release</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">Request SP2</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                          </ul>
                        </li>
                        <li>
                          <img className="img-fluid" src={'/media/ceisa/document-2.jpg'} alt={'Document 2'} />
                          <ul className="list-ic vertical">
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">Pemeriksaan Dokumen</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">SPBB</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                          </ul>
                        </li>
                        <li>
                          <img className="img-fluid" src={'/media/ceisa/document-3.jpg'} alt={'Document 3'} />
                          <ul className="list-ic vertical">
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">Search for Truck</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">Booking Truck</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">Return Container</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                          </ul>
                        </li>
                        <li className="active">
                          <img className="img-fluid" src={'/media/ceisa/document-4.jpg'} alt={'Document 4'} />
                          <ul className="list-ic vertical">
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">Searching Warehouse</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                            <li>
                              <span/>
                              <div>
                                <p className="kt-font-bold">Booking Warehouse</p>
                                <p className="kt-font-info">11/12/2019 11:12 WIB</p>
                                <p className="kt-font-danger">1 Menit</p>
                              </div>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                ) : null}
                {tab === 'pib-header' ? (
                  <div className="tab-pane active" role="tabpanel">
                    <div className="embed-responsive embed-responsive-1by1">
                      <iframe title="pdf" className="embed-responsive-item" src="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"></iframe>
                    </div>
                  </div>
                ) : null}
                {tab === 'pib-details' ? (
                  <div className="tab-pane active" role="tabpanel">
                    <div className="table-responsive">
                      <table className="table table-lg">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Product Description</th>
                            <th>Price & Tax</th>
                            <th>Amount</th>
                            <th>Document</th>
                            <th>FOB</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          {details.map((item, index) => {
                            return (
                              <tr key={index}>
                                <th scope="row">
                                  <label className={'kt-checkbox kt-checkbox--solid kt-checkbox--danger'}>
                                    <input type="checkbox" disabled/>
                                    <span></span>
                                  </label>
                                </th>
                                <td>
                                  <p>HS Code : {item.product.hsCode}</p>
                                  <p>Desc : {item.product.desc}</p>
                                  <p>({item.product.alias})</p>
                                  <p>Merk : {item.product.merk}</p>
                                </td>
                                <td>
                                  <p>Price : {item.product.price}</p>
                                  <p>Tax : {item.product.tax}</p>
                                </td>
                                <td>
                                  <p>Unit : {item.product.unit}</p>
                                  <p>Netto : {item.product.netto}</p>
                                </td>
                                <td>
                                  {item.documents.map((item, index) => {
                                    return (
                                      <div className="py-1" key={index}>
                                        <p>Type : {item.type}</p>
                                        <p>Num : {item.num}</p>
                                        <p>Date : {item.date}</p>
                                      </div>
                                    )
                                  })}
                                </td>
                                <td>
                                  <p>{item.fob}</p>
                                </td>
                                <td className="text-right">
                                  <img className="rounded" width="180px" src={'/media/products/product' + (index + 1) + '.jpg'} alt={item.desc} />
                                </td>
                              </tr>
                            )
                          })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                ) : null}
                {tab === 'billing' ? (
                  <div className="tab-pane active p-4" role="tabpanel">
                    Billing
                  </div>
                ) : null}
                {tab === 'shipments-details' ? (
                  <div className="tab-pane active p-4" role="tabpanel">
                    Shipments Details
                  </div>
                ) : null}
                {tab === 'trucking-details' ? (
                  <div className="tab-pane active p-4" role="tabpanel">
                    Trucking Details
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
