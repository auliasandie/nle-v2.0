
import React, { Component } from 'react'
import { HashRouter, Route, Switch } from 'react-router-dom';
import LayoutInbound from './views/Inbound/LayoutInbound'
import LayoutOutbond from './views/Outbound/LayoutOutbond';
import Home from './views/Home/Home';
import LayoutDomestic from './views/Domestic/LayoutDomestic';


export default class Routes extends Component {
    constructor() {
        super()
        this.state = {
        }
    }
    render() {
        return (
            <HashRouter>
                <Switch>
                  <Route exact path="/" name="Home"  component={Home} />
                  <Route path="/inbound/dashboard" component={LayoutInbound} />
                  <Route path="/inbound/cargo" component={LayoutInbound} />
                  <Route path="/inbound/pelimpahan" component={LayoutInbound} />
                  <Route path="/inbound/bookingtruck" component={LayoutInbound} />
                  <Route path="/outbound/vessel" component={LayoutOutbond} />
                  <Route path="/outbound/dashboard" component={LayoutOutbond} />
                  <Route path="/outbound/pelimpahan" component={LayoutOutbond} />
                  <Route path="/domestic/cari" component={LayoutDomestic} />
                  <Route path="/domestic/hasil" component={LayoutDomestic} />
                  <Route path="/domestic/dashboard" component={LayoutDomestic} />
                  <Route path="/domestic/pelimpahan" component={LayoutDomestic} />
                  <Route path="/vessel/list" component={LayoutDomestic} />
                  <Route path="/vessel/detail" component={LayoutDomestic} />
                  <Route path="/vessel/list" component={LayoutInbound} />
                  <Route path="/vessel/detail" component={LayoutInbound} />
                  <Route path="/vessel/list" component={LayoutOutbond} />
                  <Route path="/vessel/detail" component={LayoutOutbond} />
              </Switch>     
            </HashRouter>
           
        );
    }
}
