import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router } from 'react-router-dom'
import './assets/vendors/custom/fullcalendar/fullcalendar.bundle.css';
import './assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css';
import './assets/vendors/general/tether/dist/css/tether.css';
import './assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css';
import './assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css';
import './assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css';
import './assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css';
import './assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css';
import './assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css';
import './assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css';
import './assets/vendors/general/select2/dist/css/select2.css';
import './assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css';
import './assets/vendors/general/nouislider/distribute/nouislider.css';
import './assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css';
import './assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css';
import './assets/vendors/general/dropzone/dist/dropzone.css';
import './assets/vendors/general/summernote/dist/summernote.css';
import './assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css';
import './assets/vendors/general/animate.css/animate.css';
import './assets/vendors/general/toastr/build/toastr.css';
import './assets/vendors/general/morris.js/morris.css';
import './assets/vendors/general/sweetalert2/dist/sweetalert2.css';
import './assets/vendors/general/socicon/css/socicon.css';
import './assets/vendors/custom/vendors/line-awesome/css/line-awesome.css';
import './assets/vendors/custom/vendors/flaticon/flaticon.css';
import './assets/vendors/custom/vendors/flaticon2/flaticon.css';
import './assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css';
import './assets/css/demo7/style.bundle.css';
import './assets/media/logos/favicon.ico';
import './assets/css/digico/digico.css';
import './assets/css/clickargo/style.css';
import "./assets/css/datatable/jquery.dataTables.css";

ReactDOM.render(<Router><App /></Router>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
