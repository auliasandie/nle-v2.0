import React,{Component, Fragment} from 'react';
import Select from 'react-select';
import * as Helper from "../../../Helper";
import StarRatings from "react-star-ratings";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import { MDBDataTable } from 'mdbreact';

class Hasil extends Component{
    constructor(props){
        super(props)
        this.state = {
            vessels : [],
            partner : {},
            filter1 : 10,
            filter2 : null,
            filter3 : null,
        }
        this.fetchVessels();
        this.handlePesan = this.handlePesan.bind(this);
        this.urutkan = this.urutkan.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.toggleModal2 = this.toggleModal2.bind(this);
        this.toggleModal3 = this.toggleModal3.bind(this);
        this.toggleModal4 = this.toggleModal4.bind(this);
    }

    changeRating = ( newRating, name ) => {
        console.log(newRating, name);
    }

    handlePesan = (event) => {
        event.preventDefault();
        console.log(event)
        // this.props.history.push('/prahuhub/detail');
    }

    urutkan = (event) => {
        event.preventDefault();
        console.log(event.target);
    }

    contentSp2Header = (breadcrump) =>{
        return(
            <div className="kt-portlet__head">

                <div className="kt-portlet__head-label">
                    <h3 className="kt-portlet__head-title">
                        {
                            breadcrump.map((bcp, i) => {
                                return (
                                    <span key={i}>
                  {
                      bcp.link === null
                          ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                          : (<a href="/#/warehouse/" className="kt-link kt-font-transform-u" onClick={()=>{this.setActiveView(bcp.link)}} key={i}>{bcp.label}</a>)
                  }
                                        {
                                            i !== (breadcrump.length-1)
                                                ? " > "
                                                : null
                                        }
                  </span>
                                )
                            })
                        }
                    </h3>
                </div>
            </div>
        )
    }

    toggleModal(e)   {
        this.setState({isModalShow: !this.state.isModalShow});
    }

    toggleModal2(e)   {
        this.setState({isModalShow2: !this.state.isModalShow2});
    }

    toggleModal3(e)   {
        let vesId = e.target.getAttribute("data-vesselid");
        if(vesId!==null){
            let vessel = this.state.vessels.filter(v=>parseInt(v.id)===parseInt( vesId)) [0];
            this.setState({partner: vessel.partner});
        }
        this.setState({isModalShow3: !this.state.isModalShow3});
    }

    toggleModal4(e)   {
        this.setState({isModalShow4: !this.state.isModalShow4});
    }

    fetchVessels = async ()=> {
        await fetch("https://my-json-server.typicode.com/mudiadamz/jsonplaceholder/vessels")
            .then(response => response.json())
            .then(data =>
                this.setState({vessels: data})
            );
    }

    render(){
        const breadcrump = [
            {
                label: 'Hasil Pencarian',
                link: null
            }
        ];

        const  currentStep= 0;
        const filters1 = [];
        for (let i=1;i<11;i++){
            filters1.push({label: i, value:i});
        }
        const filters2 = [
            {label: "Kapal", value: 0},
            {label: "Pelabuhan Asal \nKota - Kawasan \nEstimasi Berangkat", value: 1},
            {label: "Pelabuhan Tujuan \nKota - Kawasan \nEstimasi Tiba", value: 2},
            {label: "Closing Time", value: 3},
            {label: "Jenis Container", value: 4},
            {label: "Jenis Pelayanan", value: 5},
            {label: "Harga", value: 6},
            {label: "Partner", value: 7},
            {label: "Action", value: 8},
        ];
        const filters3 = [
            {label: "Rendah ke tinggi", value: 0},
            {label: "Tinggi ke rendah", value: 1},
        ];

        const vessels = this.state.vessels.map(ves=>{
            let col1= <>
                <img src={ves.vessel.photo} alt="harbor" style={{width:60,height:60}} />
                <br/>
                {ves.vessel.name}
                <br/>
                <button className="btn btn-default" type="button"
                        onClick={this.toggleModal}>
                    <i className="flaticon-interface-1 icon-sm text-warning"/> Biaya Termasuk
                </button>
                </>;
            let col2=<>{ves.vessel.origin}<br/>{ves.vessel.departure}<br/>
                <button className="btn btn-default " type="button"
                        onClick={this.toggleModal2}>
                    <i className="flaticon-interface  icon-sm text-success"/> Ketentuan Barang
                </button></>;
            let col3=<>{ves.vessel.destination}<br/>{ves.vessel.arrival}<br/>
                <button className="btn btn-default " type="button"
                        onClick={this.toggleModal3} data-vesselid={ves.id}>
                    <i className="flaticon-user-ok icon-sm text-info"/> Profil Partner
                </button></>;
            let col4=<>{ves.closing}</>;
            let col5=<>{ves.containerType}</>;
            let col6=<>{ves.serviceType}</>;
            let col7=<>{
                ves.price===null?
                    <i style={{color:"blue"}}>Tanya Harga</i>:
                    <p>Rp. {Helper.convertToRupiah(ves.price)}<br/>{ves.limitedTimeOffer}</p>}</>;
            let col8=<>
                {ves.partner.name} <br/>
                <StarRatings
                    rating={ves.partner.rate}
                    changeRating={this.changeRating}
                    name='rating'
                    starDimension="10px"
                    starSpacing="0"
                    starRatedColor="gold"
                />
            </>;
            let col9=<>
                {
                    ves.price===null?'':
                        <button
                            onClick={this.handlePesan}
                            className="btn btn-warning">
                            <i className="flaticon-cart icon-sm"/>
                            Pesan</button>
                }
                <button className="btn btn-info"
                        onClick={this.toggleModal4}>
                    <i className="flaticon-questions-circular-button icon-sm"/>
                    Tanya</button>
            </>;

            return [col1,col2,col3,col4,col5,col6,col7,col8,col9];
        });

        const data = {
            columns: filters2.map(row=>{
                return{
                    label: row.label,
                    field: row.value,
                    sort: 'asc',
                }
            }),

            rows: vessels.map(ves=>{
                let row = {};
                filters2.map(col => {
                    row[col.value] = ves[col.value];
                });
                return row;
            })
        }

        return(
            <div className="kt-portlet kt-portlet--height-fluid">
                {this.contentSp2Header(breadcrump)}
                <div className="kt-portlet__body">
                    <MDBDataTable
                        striped
                        bordered
                        small
                        sortable={true}
                        data={data}
                        noBottomColumns={true}
                    />

                    <div className="row">
                        <div className="col-sm-12 text-center">
                            <button type="button" className="btn btn-default" 
                            // onClick={e=>
                            //     this.props.history.push('/prahuhub/cari')}
                                >
                                <i className="flaticon-cancel"/> Back</button>
                        </div>
                    </div>
                </div>

                <Modal isOpen={this.state.isModalShow} backdrop={true} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Biaya Termasuk</ModalHeader>
                    <ModalBody>
                        <p>Biaya Termasuk ...</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleModal}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.isModalShow2} backdrop={true} toggle={this.toggleModal2}>
                    <ModalHeader toggle={this.toggleModal2}>Ketentuan Barang</ModalHeader>
                    <ModalBody>
                        <p>Ketentuan Barang ...</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleModal2}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleModal2}>Cancel</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.isModalShow3} backdrop={true} toggle={this.toggleModal3}>
                    <ModalHeader toggle={this.toggleModal3}>Profil Partner</ModalHeader>
                    <ModalBody>
                        <table className="table table-bordered">
                            <tbody>
                            <tr>
                                <td>ID</td>
                                <td>{this.state.partner.id||''}</td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>{this.state.partner.name||''}</td>
                            </tr>
                            <tr>
                                <td>Rating</td>
                                <td>
                                    <StarRatings
                                        rating={!this.state.partner.rate?0:this.state.partner.rate}
                                        changeRating={this.changeRating}
                                        name='rating'
                                        starDimension="10px"
                                        starSpacing="0"
                                        starRatedColor="gold"
                                    />
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleModal3}>Save</Button>
                        <Button color="secondary" onClick={this.toggleModal3}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.isModalShow4} backdrop={true} toggle={this.isModalShow4}>
                    <ModalHeader toggle={this.isModalShow4}>Kirim Pertanyaan</ModalHeader>
                    <ModalBody>
                        <div className="form-group">
                            <label>Pertanyaan</label>
                            <textarea className="form-control"/>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleModal4}>Save</Button>
                        <Button color="secondary" onClick={this.toggleModal4}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }

}

export default Hasil;