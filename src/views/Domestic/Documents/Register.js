import React,{Component, Fragment} from 'react';

class Register extends Component{
    constructor(props){
        super(props);
        this.onRegSebagaiHandler = this.onRegSebagaiHandler.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            disabled: false,
            alertShow: false,
            rSebagai : {
                rs1: true,
                rs2: false
            }
        }
    }

    onRegSebagaiHandler = (event)=>{
        console.log(this.state)
    }

    handleSubmit(event) {
        event.preventDefault();

        let data = new FormData(event.target);
        data.id = "";
        this.setState({disabled: true});
        fetch('https://my-json-server.typicode.com/mudiadamz/jsonplaceholder/users', {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then(data => {
                this.setState({disabled: false, alertShow: true});

                this.props.history.push('/domestic/cari');
                // addToast('Saved Successfully', { appearance: 'success' })

            });
    }

    contentSp2Header = (breadcrump) =>{
        return(
            <div className="kt-portlet__head">

                <div className="kt-portlet__head-label">
                    <h3 className="kt-portlet__head-title">
                        {
                            breadcrump.map((bcp, i) => {
                                return (
                                    <span key={i}>
                  {
                      bcp.link === null
                          ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                          : (<a href="/#/warehouse/" className="kt-link kt-font-transform-u" onClick={()=>{this.setActiveView(bcp.link)}} key={i}>{bcp.label}</a>)
                  }
                                        {
                                            i !== (breadcrump.length-1)
                                                ? " > "
                                                : null
                                        }
                  </span>
                                )
                            })
                        }
                    </h3>
                </div>
            </div>
        )
    }

    render(){

        const breadcrump = [
            {
                label: 'Register',
                link: null
            }
        ];
        return(
            <div className="kt-portlet kt-portlet--height-fluid">
                {this.contentSp2Header(breadcrump)}
                <form autoComplete={"off"} onSubmit={this.handleSubmit} method="post">
                    <div className="kt-portlet__body">
                        <div className="kt-widget15">
                            <div className="alert alert-success"
                                 style={{display:this.state.alertShow?"block":"none"}}>
                                Successfully registered!</div>
                            <div className="form-group">
                                <label>Registrasi Sebagai</label>
                                <br/>
                                <input
                                    name="rSebagai"
                                    id="rs1"
                                    type="radio"
                                    className="ml-3"
                                    onChange={this.onRegSebagaiHandler}
                                />
                                <label htmlFor="rs1" className="ml-1">Pengirim barang</label>
                                <input
                                    name="rSebagai"
                                    id="rs2"
                                    type="radio"
                                    className="ml-3"
                                    onChange={this.onRegSebagaiHandler}
                                />
                                <label htmlFor="rs2" className="ml-1">Partner</label>
                            </div>
                            <div className="form-group">
                                <label>Nama</label>
                                <input className="form-control" name="nama" />
                            </div>
                            <div className="form-group">
                                <label>Telepon/WA</label>
                                <input className="form-control" name="telepon"/>
                            </div>
                            <div className="form-group">
                                <label>Email</label>
                                <input className="form-control" name="email"/>
                            </div>
                            <div className="form-group">
                                <label>Kata Sandi</label>
                                <input className="form-control" name="password" type="password"/>
                            </div>
                            <div className="form-group">
                                <label>Ketik Ulang Kata Sandi</label>
                                <input className="form-control" name="password2" type="password"/>
                            </div>
                            <div className="form-group">
                                <button
                                    disabled={ this.state.disabled }
                                    type="submit"
                                    className={
                                        this.state.disabled?
                                        "btn btn-info spinner spinner-white spinner-right":
                                        "btn btn-info"}>Registrasi</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }

}

export default Register;

