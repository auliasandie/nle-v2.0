import React, { Component } from 'react'
import { Dropdown, Form, Col, Button } from 'react-bootstrap';
import { NavLink, HashRouter, Route, Switch, Link } from 'react-router-dom';
import Dashboard from './Dashboard/Dashboard';
import Pelimpahan from './Pelimpahan/Pelimpahan';
import DocumentVessel from './Vessel/DocumentVessel'

export default class LayoutOutbond extends Component {
    constructor() {
        super()
        this.state = {
            showMobileSideMenu: false,
            isLoadingContent: false,
            activepage: 'datatable',
            activeDocuments: false,
            dataUserPortal: {
                npwp: null,
                nama_perusahaan: null,
                alamat: null,
                telp: null,
                nama_kontak_person: null,
                email: null
              },
            isContent: false

        }
    }

    handleHeaderMobileToggler = (ev) => {
        this.setState({ showMobileSideMenu: true });
    }
    handleCloseHeaderMobileToggler = (ev) => {
        this.setState({ showMobileSideMenu: false });
    }



    render() {

        return (
            <HashRouter>
                <div className={"kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading" + (this.state.showMobileSideMenu ? " kt-header-menu-wrapper--on" : null)}>
                    <div id="kt_header_mobile" className="kt-header-mobile  kt-header-mobile--fixed" style={{ backgroundColor: "#051a92" }}>
                        <div className="kt-header-mobile__logo">
                        <NavLink to="/">
                        <img width="240px" height="70px" src={require('../assets/image/logo-icon-transparent.png')} alt="NLE"></img>
                            </NavLink>
                        </div>
                        <div className="kt-header-mobile__toolbar">
                            <button className="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler" onClick={(ev) => this.handleHeaderMobileToggler(ev)}><span></span></button>
                        </div>
                    </div>
                    <div className="kt-grid kt-grid--hor kt-grid--root">
                        <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

                            {/*<!-- begin:: Aside -->*/}
                            <button className="kt-aside-close " id="kt_aside_close_btn" onClick={(ev) => this.handleCloseHeaderMobileToggler(ev)}><i className="la la-close"></i></button>
                            <div className={this.state.showMobileSideMenu ? "kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid kt-header-menu-wrapper--on" : "kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"} id="kt_aside">

                                {/*<!-- begin:: Brand -->*/}
                                <div className="kt-aside__brand kt-grid__item  " id="kt_aside_brand" style={{ backgroundColor: "#051a92" }}>
                                <NavLink to='/'>
                                <img width="130%" src={require('../assets/image/logo-icon-transparent.png')} alt="NLE" style={{ marginLeft: "-22px" }}></img>
                                    </NavLink>
                                </div>
                                {/*<!-- end:: Brand -->*/}

                                {/*<!-- begin:: Aside Menu -->*/}
                                <div className="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper" style={{ height: "100%", overflow: "auto" }}>
                                    <div id="kt_aside_menu" className="kt-aside-menu  kt-aside-menu--dropdown " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
                                        <ul className="kt-menu__nav ">
                                            <li className="kt-menu__item" aria-haspopup="true">
                                                <Link to="/outbound/dashboard" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                                    <i className="kt-menu__link-icon flaticon2-protection"></i>
                                                    <span className="kt-menu__link-text">Dashboard</span>
                                                </Link>
                                            </li>
                                            <li className="kt-menu__item" aria-haspopup="true">
                                                <Link to="/outbound/pelimpahan" className="kt-menu__link" onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                                    <i className="kt-menu__link-icon flaticon-multimedia-2"></i>
                                                    <span className="kt-menu__link-text">Pelimpahan</span>
                                                </Link>
                                            </li>
                                            <li className="kt-menu__item" aria-haspopup="true">
                                                <Link to="/outbound/vessel" className="kt-menu__link" onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                                    <i className="kt-menu__link-icon flaticon-multimedia-2"></i>
                                                    <span className="kt-menu__link-text">Vessel</span>
                                                </Link>
                                            </li>
                                            <li className="kt-menu__item" aria-haspopup="true">
                                                <Link to="/outbound/statics" className="kt-menu__link" onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                                    <i className="kt-menu__link-icon flaticon-multimedia-2"></i>
                                                    <span className="kt-menu__link-text">Statics</span>
                                                </Link>
                                            </li>

                                            <li className="kt-menu__item kt-menu__item--submenu kt-menu__item--bottom-2" aria-haspopup="true">
                                                <span className="kt-menu__link-text-bottom"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                {/*<!-- end:: Aside Menu -->*/}
                            </div>
                            <div className="kt-aside-menu-overlay"></div>

                            {/*<!-- end:: Aside -->*/}
                            <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                                {/*<!-- begin:: Header -->*/}
                                <div id="kt_header" className="kt-header kt-grid kt-grid--ver  kt-header--fixed ">

                                    {/*<!-- begin: Header Menu -->*/}
                                    <button className="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn" onClick={(ev) => this.handleCloseHeaderMobileToggler(ev)}><i className="la la-close"></i></button>
                                    <div className="kt-header__topbar"></div>
                                    <div className="kt-header__topbar">
                        <div className="kt-header__topbar-item kt-header__topbar-item--search">
                            <div className="kt-header__topbar-wrapper">
                                <div className="kt-quick-search" id="kt_quick_search_default">
                                    <form method="get" className="kt-quick-search__form">
                                        <div className="input-group">
                                            <div className="input-group-prepend"><span className="input-group-text"><i className="flaticon2-search-1" /></span></div>
                                            <input type="text" className="form-control kt-quick-search__input" placeholder="Search anything..." />
                                            <div className="input-group-append"><span className="input-group-text"><i className="la la-close kt-quick-search__close" /></span></div>
                                        </div>
                                    </form>
                                    <div id="kt_quick_search_toggle" data-toggle="dropdown" data-offset="10px, 0px" />
                                    <div className="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
                                        <div className="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height={300} data-mobile-height={200}>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                                    {/*<!-- end: Header Menu -->*/}

                                    {/*<!-- begin:: Header Topbar -->*/}

                                    <div className="kt-header__topbar">

                                        {/*<!--begin: Notifications -->*/}
                                        <div className="kt-header__topbar-item kt-header__topbar-item--user">
                                            {/* <div className="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                          <span className="kt-header__topbar-icon kt-header__topbar-icon--success"><i className="flaticon2-bell-alarm-symbol"></i></span>
                          <span className="kt-hidden kt-badge kt-badge--danger"></span>
                        </div> */}
                                            <Dropdown className="kt-header__topbar-wrapper">
                                                <Dropdown.Toggle
                                                    variant="success"
                                                    id="dropdown-basic"
                                                    className="btn btn-outline-brand btn-icon btn-circle"
                                                    style={{ marginTop: "10px", backgroundColor: "white", borderColor: "" }}
                                                >
                                                    <i style={{ color: "#051a92", width: "10px" }} className="flaticon-bell"></i>
                                                </Dropdown.Toggle>

                                                <Dropdown.Menu style={{ marginTop: "15px" }} className="kt-menu__nav ">
                                                    {/* href="#/action-1" */}
                                                    <Dropdown.Item className="kt-nav__item" >Action</Dropdown.Item>
                                                    <Dropdown.Item className="kt-nav__item">Another action</Dropdown.Item>
                                                    <Dropdown.Item className="kt-nav__item">Something else</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </div>
                                        {/*<!--end: Notifications -->*/}

                                        {/*<!--begin: User bar -->*/}
                                        <div className="kt-header__topbar-item kt-header__topbar-item--user">

                                            <Dropdown className="kt-header__topbar-wrapper">
                                                <Dropdown.Toggle
                                                    variant="success"
                                                    id="dropdown-basic"
                                                    className="btn btn-outline-brand btn-icon btn-circle"
                                                    style={{ marginTop: "10px", backgroundColor: "white", borderColor: "none" }}
                                                >
                                                    <i style={{ color: "#051a92", width: "10px" }} className="flaticon2-user-outline-symbol"></i>
                                                </Dropdown.Toggle>

                                                <Dropdown.Menu style={{ marginTop: "15px" }} className="kt-menu__nav ">
                                                    {/* href="#/action-1" */}
                                                    <Dropdown.Item className="kt-nav__item" >My Profile</Dropdown.Item>
                                                    <Dropdown.Item className="kt-nav__item">Account settings and more</Dropdown.Item>
                                                    <Dropdown.Item className="kt-nav__item" >Sign Out</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </div>

                                        {/*<!--end: User bar -->*/}

                                    </div>
                                    {/*<!-- end:: Header Topbar -->*/}
                                </div>
                                {/*<!-- end:: Header -->*/}

                                <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                                    {/*<!-- begin:: Content -->*/}

                                    {/*<!-- begin:: Content Dashboard -->*/}
                                    <div className="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                                       
                                
                                       
                                        {
                                            this.state.isLoadingContent ? (
                                                <span>Loadings ...</span>
                                            ) : (
                                                    <Switch>
                                                        <Route path="/outbound/vessel" component={DocumentVessel}/>
                                                        <Route path="/outbound/dashboard" component={Dashboard}/>
                                                        <Route path="/outbound/pelimpahan" component={Pelimpahan}/>
                                                    </Switch>
                                                )
                                        }


                                    </div>
                                    {/*<!-- end:: Content Dashboard -->*/}

                                    {/*<!-- end:: Content -->*/}

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </HashRouter>
        );
    }
}
