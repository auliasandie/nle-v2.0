import React, { useState, Fragment, useEffect } from 'react'
import { Link } from 'react-router-dom'
import Carousel from 'nuka-carousel'
import dashforgeCss from '../BookingRequest/dashforge.module.css'

function Detail(props) {

    const routeProps = {
        location: props.location
    };

    const [isHazmat, setIsHazmat] = useState(false);

    const BRDetails = routeProps.location.state.arrVesselDetail.booking_requests[0].details

    // const schedule = routeProps.location.state.arrVesselDetail.details.schedule

    const containers = routeProps.location.state.arrVesselDetail.containers

    console.log(routeProps.location.state.arrVesselDetail)

    console.log(routeProps.location.state.arrVesselDetail.tracking_status.freight[0].clickargo.subStatus)

    useEffect(() => {
        //set hazmat
        if (routeProps.location.state.arrVesselDetail.details.product_type == 'general') {
            setIsHazmat(false)
        } else if (routeProps.location.state.arrVesselDetail.details.product_type == 'special') {
            setIsHazmat(true)
        } else if (routeProps.location.state.arrVesselDetail.details.product_type == 'dangerous') {
            setIsHazmat(true)
        } else if (routeProps.location.state.arrVesselDetail.details.product_type == 'others') {
            setIsHazmat(false)
        }
    }, [])

    return (
        <>
            <div style={{ marginBottom: "10px" }}>
                <div style={{ float: "left" }}>
                    <h5 style={{ display: "inline" }} className={dashforgeCss.txBlack}>Tracking Information</h5>
                </div>
            </div>

            <br />
            <br />

            <div style={{ marginBottom: "10px" }}>
                <Link to="/vessel/list" style={{ display: "inline", color: "#2d9ff7" }} >
                    Back
                </Link>
            </div>

            <div className="kt-portlet">
                <div className="kt-portlet__body" style={{ flexGrow: '0' }}>
                    <div className="row">
                        <ul className="list-ic horizontal">
                            <li>
                                <img
                                    className="img-fluid"
                                    src={window.location.origin + '/assets/images/clickargo/trucking/warehouse.svg'}
                                    style={{ width: '70px' }}
                                    alt={'Document 1'} />
                                <ul className="list-ic vertical">

                                </ul>
                                <ul className="list-ic vertical">

                                </ul>
                            </li>

                            <li>
                                <img
                                    className="img-fluid"
                                    src={window.location.origin + '/assets/images/clickargo/trucking/port-origin.svg'}
                                    style={{ width: '70px' }}
                                    alt={'Document 3'} />
                                <ul className="list-ic vertical">
                                    {/* {contentTrackingTruck} */}
                                </ul>
                            </li>
                            <li className="active">
                                <img
                                    className="img-fluid"
                                    src={window.location.origin + '/assets/images/clickargo/trucking/vessel.svg'}
                                    style={{ width: '70px', height: '70px' }}
                                    alt={'Document 4'} />
                                <ul className="list-ic vertical">
                                    {routeProps.location.state.arrVesselDetail.tracking_status.freight[0].clickargo.subStatus.map((input, index) => (
                                    <li>
                                        <span />
                                        <div>
                                            <p className="kt-font-bold">{input.name}</p>
                                            <p className="kt-font-info">{input.value.start} <label className={dashforgeCss.txDanger}>-</label> {input.value.end}</p>
                                        </div>
                                    </li>
                                    ))}
                                </ul>
                            </li>
                            <li className="active">
                                <img
                                    className="img-fluid"
                                    src={window.location.origin + '/assets/images/clickargo/trucking/port-destination.svg'}
                                    style={{ width: '70px' }}
                                    alt={'Document 5'} />
                                <ul className="list-ic vertical">

                                </ul>
                            </li>
                            {/* <li className="active">
                                    <img
                                        className="img-fluid"
                                        src={window.location.origin + '/assets/images/clickargo/trucking/mover-truck.svg'}
                                        style={{ width: '70px' }}
                                        alt={'Document 5'} />
                                    <ul className="list-ic vertical">

                                    </ul>
                                </li> */}
                        </ul>

                    </div>
                </div>
            </div>

            <div style={{ marginBottom: "10px" }}>
                <div style={{ float: "left" }}>
                    <h5 style={{ display: "inline" }} className={dashforgeCss.txBlack}>Details Information</h5>
                </div>
            </div>

            <br />
            <br />

            <div className="kt-portlet">
                <div className="kt-portlet__body" style={{ flexGrow: '0' }}>
                    <div className="row" style={{marginBottom: '15px'}}>

                        <div className="col-md-12" style={{marginBottom: '15px'}}>
                            <h5 style={{ color: '#5bd874' }}>General</h5>
                        </div>

                        <div className="col-md-3">
                            <div className="form-group">
                                <label>Shipper</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.shipper_name}</h6>
                            </div>
                        </div>

                        <div className="col-md-3">
                            <div className="form-group">
                                <label>Address</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.shipper_address}</h6>
                            </div>
                        </div>

                        <div className="col-md-3">
                            <div className="form-group">
                                <label>Carrier</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.carrier}</h6>
                            </div>
                        </div>

                        <div className="col-md-3">
                            <div className="form-group">
                                <label>Address</label>
                                <h6 className={dashforgeCss.txBlack}>-</h6>
                            </div>
                        </div>

                        <div className="col-md-3">
                            <div className="form-group">
                                <label>Forwarder</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.forwarder}</h6>
                            </div>
                        </div>

                        <div className="col-md-3">
                            <div className="form-group">
                                <label>Address</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.forwarder_address}</h6>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Consignee</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.consignee}</h6>
                            </div>
                        </div>

                    </div>

                    <div className="row" style={{marginBottom: '15px'}}>

                        <div className="col-md-12" style={{marginBottom: '15px'}}>
                            <h5 style={{ color: '#5bd874' }}>Transport</h5>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label>Vessels</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.main_carriage.vessel}</h6>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label>Voyage</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.main_carriage.voyage}</h6>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label>Move Type</label>
                                <h6 className={dashforgeCss.txBlack}>{routeProps.location.state.arrVesselDetail.move_type}</h6>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Shipment Type</label>
                                <h6 className={dashforgeCss.txBlack}>{routeProps.location.state.arrVesselDetail.details.shipment_type}</h6>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label>Origin Goods</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.origin_port_name} ({BRDetails.origin_unloc})</h6>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label>Place Of Receipt</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.origin_port_name} ({BRDetails.origin_unloc})</h6>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label>Port Of Load</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.main_carriage.port_of_load_name} ({BRDetails.main_carriage.port_of_load})</h6>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label>Port Of Discharge</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.main_carriage.port_of_discharge_name} ({BRDetails.main_carriage.port_of_discharge})</h6>
                            </div>
                        </div>

                        <div className="col-md-3">
                            <div className="form-group">
                                <label>Destination (Place of Delivery)</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.destination_port_name} ({BRDetails.destination_unloc})</h6>
                            </div>
                        </div>

                    </div>

                    <div className="row" style={{marginBottom: '15px'}}>
                        <div className="col-md-12" style={{marginBottom: '15px'}}>
                            <h5 style={{ color: '#5bd874' }}>Customer Compliance (Goverment Tax IDs)</h5>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label>Shipper</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.shipper_name}</h6>
                            </div>
                        </div>
                        <div className="col-md-10">
                            <div className="form-group">
                                <label>Consignee</label>
                                <h6 className={dashforgeCss.txBlack}>{BRDetails.consignee}</h6>
                            </div>
                        </div>
                    </div>

                    <div className="row" style={{marginBottom: '15px'}}>
                        <div className="col-md-12" style={{marginBottom: '15px'}}>
                            <h5 style={{ color: '#5bd874' }}>Container</h5>
                        </div>

                        <Carousel heightMode={`current`}>
                            {containers.map((inputContainer, indexContainer) => (
                                <Fragment key={`${inputContainer}~${indexContainer}`}>
                                    <div className="col-md-12">
                                        <fieldset className={dashforgeCss.formFieldset} style={{marginBottom: '15px'}}>

                                            <div className="row" style={{marginBottom: '15px'}}>
                                                <div className="col-md-2">
                                                    <label>Number</label>
                                                    <h6 className={dashforgeCss.txBlack}>UNIS12356789</h6>
                                                </div>
                                                <div className="col-md-2">
                                                    <label>Type</label>
                                                    <h6 className={dashforgeCss.txBlack}>{inputContainer.name}</h6>
                                                </div>
                                                <div className="col-md-2">
                                                    <label>Seal Number</label>
                                                    <h6 className={dashforgeCss.txBlack}>1234561223</h6>
                                                </div>
                                                <div className="col-md-2">
                                                    <label>Seal Type</label>
                                                    <h6 className={dashforgeCss.txBlack}>carrier</h6>
                                                </div>
                                                <div className="col-md-4">
                                                    <label>Comments</label>
                                                    <h6 className={dashforgeCss.txBlack}>{inputContainer.attribute.container_comments}</h6>
                                                </div>
                                            </div>

                                            <div className="row" style={{marginBottom: '15px'}}>
                                                <div className="col-md-12">
                                                    <label>Cargo</label>
                                                </div>

                                                {inputContainer.cargos.map((inputCargo, indexCargo) => (
                                                    <Fragment key={`${inputCargo}~${indexCargo}`}>
                                                        <div className="col-md-4">
                                                            <fieldset className={dashforgeCss.formFieldset} style={{marginBottom: '15px', backgroundColor: '#f2f7fd'}}>
                                                                <div className="row">
                                                                    <div className="col-2">
                                                                        <div className="form-group cargo_description_div">
                                                                            <h6 className={dashforgeCss.txBlack}>{indexCargo + 1}</h6>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-5">
                                                                        <div className="form-group cargo_description_div">
                                                                            <label>Description</label>
                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.description}</h6>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-5">
                                                                        <div className="form-group cargo_description_div">
                                                                            <label>Package Count</label>
                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.quantity}</h6>
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-2">
                                                                        <div className="form-group cargo_description_div">
                                                                            {/* <h6 className={dashforgeCss.txBlack}>2</h6> */}
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-5">
                                                                        <div className="form-group cargo_description_div">
                                                                            <label>HS Code</label>
                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.hs_code}</h6>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-5">
                                                                        <div className="form-group cargo_description_div">
                                                                            <label>Package Type</label>
                                                                            <h6 className={dashforgeCss.txBlack}>1</h6>
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-2">
                                                                        <div className="form-group cargo_description_div">
                                                                            {/* <h6 className={dashforgeCss.txBlack}>2</h6> */}
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-5">
                                                                        <div className="form-group cargo_description_div">
                                                                            <label>Cargo Weight</label>
                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.weight} {inputCargo.weight_uom}</h6>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-5">
                                                                        <div className="form-group cargo_description_div">
                                                                            <label>Gross Volume</label>
                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.gross_volume} {inputCargo.gross_volume_uom}</h6>
                                                                        </div>
                                                                    </div>

                                                                    {isHazmat && <Fragment>
                                                                        <div className="col-12" style={{marginTop: '15px'}}>
                                                                            <div className="form-group cargo_description_div">
                                                                                <h6 style={{ color: 'red' }}>Hazmat Details</h6>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-2">
                                                                            <div className="form-group cargo_description_div">
                                                                                {/* <label style={{color: 'red'}}>Hazmat</label> */}
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-5">
                                                                            <div className="form-group cargo_description_div">
                                                                                <label>Primary IMO Class</label>
                                                                                <h6 className={dashforgeCss.txBlack}>{JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_imo}</h6>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-5">
                                                                            <div className="form-group cargo_description_div">
                                                                                <label>UNDG Number</label>
                                                                                <h6 className={dashforgeCss.txBlack}>{JSON.parse(inputCargo.attribute.replace(/\\/g, "")).undg_number}</h6>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-2">
                                                                            <div className="form-group cargo_description_div">
                                                                                {/* <label style={{color: 'red'}}>Hazmat</label> */}
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-5">
                                                                            <div className="form-group cargo_description_div">
                                                                                <label>Packing Group</label>
                                                                                <h6 className={dashforgeCss.txBlack}>{JSON.parse(inputCargo.attribute.replace(/\\/g, "")).packing_group}</h6>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-5">
                                                                            <div className="form-group cargo_description_div">
                                                                                <label>Emergency Contact</label>
                                                                                <h6 className={dashforgeCss.txBlack}>{JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_name}</h6>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-2">
                                                                            <div className="form-group cargo_description_div">
                                                                                {/* <label style={{color: 'red'}}>Hazmat</label> */}
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-5">
                                                                            <div className="form-group cargo_description_div">
                                                                                <label>Emergency Number</label>
                                                                                <h6 className={dashforgeCss.txBlack}>{JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_number}</h6>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-5">
                                                                            <div className="form-group cargo_description_div">
                                                                                <label>Proper Shipping Name</label>
                                                                                <h6 className={dashforgeCss.txBlack}>{JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_proper_shipping}</h6>
                                                                            </div>
                                                                        </div>
                                                                    </Fragment>}

                                                                </div>
                                                            </fieldset>
                                                        </div>

                                                    </Fragment>
                                                ))}


                                            </div>

                                        </fieldset>
                                    </div>
                                </Fragment>
                            ))}
                        </Carousel>

                    </div>

                    <div className="row" style={{marginBottom: '15px'}}>
                        <div className="col-md-12" style={{marginBottom: '15px'}}>
                            <h5 style={{ color: '#5bd874' }}>Document and Freight Charges</h5>
                        </div>

                        {BRDetails.payment_detail.map((inputPaymentDetail, indexPaymentDetail) => (<Fragment>
                            <div className="col-md-2">
                                <label>Freight Terms</label>
                                <h6 className={dashforgeCss.txBlack}>{inputPaymentDetail.freight_term}</h6>
                            </div>
                            <div className="col-md-2">
                                <label>Payer</label>
                                <h6 className={dashforgeCss.txBlack}>{inputPaymentDetail.payer}</h6>
                            </div>
                            <div className="col-md-8">
                                <label>Payment Location</label>
                                <h6 className={dashforgeCss.txBlack}>{inputPaymentDetail.payment_location}</h6>
                            </div>
                        </Fragment>
                        ))}
                    </div>

                    <div className="row" style={{marginBottom: '15px'}}>
                        <div className="col-md-12" style={{marginBottom: '15px'}}>
                            <h5 style={{ color: '#5bd874' }}>B/L Print Instruction (Provider to the Carrier, not printed on Bill of Loading)</h5>
                        </div>

                        <div className="col-md-2">
                            <label>B/L Type</label>
                            <h6 className={dashforgeCss.txBlack}>Pre Paid</h6>
                        </div>
                        <div className="col-md-2">
                            <label>Freighted</label>
                            <h6 className={dashforgeCss.txBlack}>Original</h6>
                        </div>
                        <div className="col-md-8">
                            <label>Unfreighted</label>
                            <h6 className={dashforgeCss.txBlack}>Original</h6>
                        </div>
                    </div>

                    <div className="row" style={{marginBottom: '15px'}}>
                        <div className="col-md-12" style={{marginBottom: '15px'}}>
                            <h5 style={{ color: '#5bd874' }}>Notification Emails</h5>
                        </div>
                        <div className="col-md-12">
                            <label>Partner Notification Email</label>
                            <h6 className={dashforgeCss.txBlack}>{BRDetails.partner_email}</h6>
                        </div>
                    </div>

                </div>
            </div>
        </>
    )
}

export default Detail
