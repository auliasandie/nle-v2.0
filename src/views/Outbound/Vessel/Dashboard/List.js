import React, { useState, Fragment, useEffect } from 'react'
import DataTable from 'react-data-table-component';
import { Button } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom'
import axios from 'axios'
import * as Constants from '../../../../Constants.js'

function List(props) {

    const [arrVesselList, setArrVesselList] = useState([]);

    useEffect(() => {
        axios
            .get(Constants.NLE_VESSEL_LIST+'?npwp='+'018245894059000')
            .then(response => {
                console.log('success get job vessel list')
                const filterVesselIdPlatform = response.data.filter(vessel => vessel.id_platform == 'PL002')
                const filterJobNumber = filterVesselIdPlatform.filter(vessel => vessel.job_number != '245345345')
                setArrVesselList(filterJobNumber)
            })
            .catch(error => {
                console.log('failed get job vessel list')
                console.log(error)
            })
    }, [])

    const moveToDetails = (event, job_number) => {
        event.preventDefault()

        const headers = {
            'Content-Type': 'application/json',
        }

        axios
            .post(Constants.CLICKARGO_FIND_SCHEDULE_BY_JOB_NUMBER, {
                job_number: job_number
            }, {
                headers: headers
            })
            .then(response => {
                console.log('success get detail vessel')
                props.history.push({
                    pathname: "/vessel/detail",
                    state: {
                        arrVesselDetail: response.data.data
                    }
                })
            })
            .catch(error => {
                console.log(error.response.data.message)
            })
    }

    const columns = [
        {
            name: 'Job Number',
            selector: 'job_number',
            sortable: true,
        },
        {
            name: 'Vessel',
            selector: 'vessel',
            sortable: true,
        },
        {
            name: 'Created At',
            selector: 'created_at',
            sortable: true,
        },
        {
            name: 'Status',
            selector: 'status',
            sortable: true,
        },
        {
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
            cell: (selector) => <div>
                <Button
                    type="button"
                    color="default"
                    className="clickargo-button"
                    onClick={event => moveToDetails(event, selector.job_number)}
                >
                    <img src={window.location.origin + '/assets/images/Clickargo2.png'} /><span>Details</span>
                </Button >
            </div>,
        },
    ]

    const datas = arrVesselList.sort((a, b) => b.id_job_vessel - a.id_job_vessel).map(obj => {
        return {
            job_number: obj.job_number,
            vessel: obj.vessel_number,
            created_at: obj.tanggal,
            status: obj.status
        }
    })

    return (
        <Fragment>
            <div className="kt-portlet kt-portlet--height-fluid">
                <div className="kt-portlet__head">
                    <div className="kt-portlet__head-label">
                        <h3 className="kt-portlet__head-title">
                            <span>

                            </span>
                        </h3>
                    </div>
                </div>
                <DataTable
                    columns={columns}
                    data={datas}
                    pagination
                    paginationServer
                    subHeader={true}
                    subHeaderAlign={"left"}
                    subHeaderComponent={
                        (
                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                <label>Search </label>
                                <input type="text" className="form-control" id="outlined-basic" label="Search" variant="outlined" size="small" style={{ margin: '5px' }} />
                            </div>
                        )
                    }
                />
            </div>
        </Fragment>
    )
}

export default withRouter(List)
