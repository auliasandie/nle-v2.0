import React, { Component } from 'react'
import { Button } from 'reactstrap'
import BootstrapTable from 'react-bootstrap-table-next'
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css"
import paginationFactory from 'react-bootstrap-table2-paginator'
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css'
import Spinner from 'react-bootstrap/Spinner'
import axios from 'axios'
import Swal from 'sweetalert2'
import { Redirect } from 'react-router'
import { withRouter } from 'react-router-dom'
import moment from 'moment'
import * as Constants from '../../../Constants'

class Schedule extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isLoading: false,
            redirect: false,
            inputFields: [],
            inputFieldsContainer: [
                {
                    //container
                    containerType: '',
                    containerTypeLabel: 'Please Select Container Type',
                    containerComments: '',
                    //container cargo
                    cargo: [
                        {
                            cargoDescription: '',
                            cargoHsCode: '',
                            cargoHsCodeLabel: 'Enter Number',
                            cargoWeight: '',
                            cargoWeightType: 'KGM',
                            cargoPackageCount: '',
                            cargoPackageType: '',
                            cargoPackageTypeLabel: 'Please select package type',
                            cargoGrossVolume: '',
                            cargoGrossVolumeType: 'MTQ',
                            cargoPrimaryImoClass: '',
                            cargoPrimaryImoClassLabel: 'Please select IMO Class',
                            cargoUndgNumber: '',
                            cargoPackingGroup: '',
                            cargoPackingGroupLabel: 'Please select Packing Group',
                            cargoProperShippingName: '',
                            cargoEmergencyContactName: '',
                            cargoEmergencyContactNumber: '',
                        }
                    ]
                }
            ],
            inputFieldsPaymentDetails: [
                {
                    paymentDetailsChangeType: '',
                    paymentDetailsChangeTypeLabel: 'Select one',
                    paymentDetailsFreightTerm: '',
                    paymentDetailsFreightTermLabel: 'Select one',
                    paymentDetailsPayer: '',
                    paymentDetailsPayerLabel: 'Select one',
                    paymentDetailsPaymentLocation: '',
                    paymentDetailsPaymentLocationLabel: 'Enter Location',
                }
            ]
        }

        this.durationFormatter = this.durationFormatter.bind(this)

        this.actionFormatter = this.actionFormatter.bind(this)

        this.submitHandler = this.submitHandler.bind(this)
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.dataSchedule !== this.props.dataSchedule) {
            setTimeout(() => {
                this.setState({ isLoading: true })
            }, 1500);
        }
    }

    submitHandler = (uuid) => {
        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        setTimeout(() => {

            axios
                .post(Constants.CLICKARGO_CHECK_NPWP, {
                    tax_number: this.props.dataUserPortal.npwp,
                    email: this.props.dataUserPortal.email,
                    // tax_number: '012345678912',
                    // email: 'testnle1@gmail.com'
                }, {
                    headers: headers
                })
                .then(responseNPWP => {
                    if (responseNPWP.data.success === true) {
                        //find schedule by uuid
                        axios
                            .post(Constants.CLICKARGO_FIND_SCHEDULE, {
                                uuid: uuid
                            })
                            .then(responseSchedule => {
                                this.setState({
                                    inputFields: [...this.state.inputFields, {
                                        shipmentType: '',
                                        shipmentTypeLabel: 'Please Select Shipment Type',
                                        productType: '',
                                        productTypeLabel: 'Please Select Product Type',
                                        //transport
                                        transportMoveType: this.props.dataMoveType,
                                        transportMoveTypeLabel: this.props.dataMoveTypeLabel,
                                        transportPOCReceipt: responseSchedule.data.data.origin_port_name + ' (' + responseSchedule.data.data.origin_unloc + ')',
                                        transportPOCDelivery: responseSchedule.data.data.destination_port_name + ' (' + responseSchedule.data.data.destination_unloc + ')',
                                        transportEarliestDepartureDate: moment(responseSchedule.data.data.origin_departure_date).format('DD/MM/YYYY'),
                                        transportLatestDeliveryDate: moment(responseSchedule.data.data.destination_arrival_date).format('DD/MM/YYYY'),
                                        //pre carriage
                                        preCarriageStart: '',
                                        preCarriageStartLabel: 'Enter Location',
                                        preCarriageMode: '',
                                        preCarriageModeLabel: 'Please choose transport mode',
                                        preCarriageETD: '',
                                        preCarriageETA: '',
                                        //main carriage
                                        mainCarriagePortOfLoading: responseSchedule.data.data.origin_unloc,
                                        mainCarriagePortOfLoadingName: responseSchedule.data.data.origin_port_name, //update main carriage
                                        mainCarriagePortOfLoadingCountry: responseSchedule.data.data.origin_country, //update main carriage
                                        mainCarriagePortOfDischarge: responseSchedule.data.data.destination_unloc,
                                        mainCarriagePortOfDischargeName: responseSchedule.data.data.destination_port_name,//update main carriage
                                        mainCarriagePortOfDischargeCountry: responseSchedule.data.data.destination_country,//update main carriage
                                        mainCarriageVessel: responseSchedule.data.data.vessel_name,
                                        mainCarriageVoyage: responseSchedule.data.data.voyage_name,
                                        mainCarriageETD: responseSchedule.data.data.origin_departure_date,
                                        mainCarriageETA: responseSchedule.data.data.destination_arrival_date,
                                        //on carriage
                                        onCarriageStart: '',
                                        onCarriageStartLabel: 'Enter Location',
                                        onCarriageMode: '',
                                        onCarriageModeLabel: 'Please choose transport mode',
                                        onCarriageETD: '',
                                        onCarriageETA: '',
                                        //general details
                                        generalDetailsCarrier: '',
                                        generalDetailsCarrierLabel: 'Please Select', //update general details
                                        generalDetailsCarrierReceipt: responseSchedule.data.data.origin_unloc, //update general details
                                        generalDetailsCarrierDelivery: responseSchedule.data.data.destination_unloc, //update general details
                                        generalDetailsEarliestDeparture: responseSchedule.data.data.origin_departure_date, //update general details
                                        generalDetailsLatestDelivery: responseSchedule.data.data.destination_arrival_date, //update general details
                                        generalDetailsBookingOffice: '',
                                        generalDetailsBookingOfficeLabel: 'Enter Location',
                                        generalDetailsContractNumber: '',
                                        //parties
                                        partiesShipperName: responseNPWP.data.data.name,
                                        partiesShipperEmail: responseNPWP.data.data.email,
                                        partiesShipperTaxNumber: '',
                                        partiesShipperAddress : '', //update parties //16-05-2010
                                        partiesShipperCountryCode: '', //update parties //16-05-2010
                                        partiesShipperCountryName: 'Shipper Country', //update parties //16-05-2010
                                        partiesShipperCity: '', //update parties //16-05-2010
                                        partiesShipperPostalCode: '', //update parties //16-05-2010
                                        partiesForwarder: 'PT GATOTKACA TRANS SYSTEMINDO',
                                        partiesConsignee: '',
                                        partiesConsigneeAddress: '', //update parties
                                        partiesConsigneeCountryCode: '', //update parties
                                        partiesConsigneeCountryName: 'Consignee Country', //update parties
                                        partiesConsigneeCity: '', //update parties
                                        partiesConsigneePostalCode: '', //update parties
                                        partiesShipperInttraId: responseNPWP.data.data.inttra_id, //update parties
                                        partiesForwarderInttraId: responseNPWP.data.data.inttra_id, //update parties
                                        partiesForwarderAddress: 'JL. ENGGO NO. 40C RT.008 RW. 016 TANJUNG PRIOK JAKARTA UTARA, DKI JAKARTA, 14310 INDONESIA', //update parties
                                        //additional party
                                        additionalPartyContractParty: '',
                                        additionalPartyNotifyParty1: '',
                                        additionalPartyNotifyParty: '',
                                        additionalPartyNotifyParty2: '',
                                        //references
                                        referencesShipperRefNumber: '',
                                        referencesForwarderRefNumber: '',
                                        referencesPurchaseOrderNumber: '',
                                        referencesConsigneeNumber:'', //update references
                                        referencesBLNumber:'', //update references
                                        referencesContractParty:'', //update references
                                        //comments and notifications
                                        customerComments: '',
                                        partnerEmailNotifications: '',
                                        //update new field schedule
                                        scheduleOriginUnloc: responseSchedule.data.data.origin_unloc,
                                        scheduleOriginPortName: responseSchedule.data.data.origin_port_name,
                                        scheduleOriginCountry: responseSchedule.data.data.origin_country,
                                        scheduleDestinationUnloc: responseSchedule.data.data.destination_unloc,
                                        scheduleDestinationPortName: responseSchedule.data.data.destination_port_name,
                                        scheduleDestinationCountry: responseSchedule.data.data.destination_country,
                                    }]
                                })

                                //save header token
                                localStorage.setItem('ckHeaderKey', responseNPWP.data.data.key)
                                localStorage.setItem('ckHeaderSecret', responseNPWP.data.data.secret)
                                localStorage.setItem('ckHeaderToken', responseNPWP.data.data.token)

                                this.props.history.push({
                                    pathname: '/booking_request_back',
                                    state: {
                                        generalArr: this.state.inputFields,
                                        containerArr: this.state.inputFieldsContainer,
                                        paymentDetailsArr: this.state.inputFieldsPaymentDetails
                                    }
                                })

                            })
                            .catch(error => {
                                if (error.response) {
                                    console.log(error.response.data.message)
                                }
                            })
                    }
                })
                .catch(error => {
                    if (error.response) {
                        if (error.response.data.success === false) {
                            Swal.fire({
                                title: 'Sorry',
                                text: "You need to register first before using the platform. Just a simple step! Lets go :)",
                                icon: 'error',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes!'
                            }).then((result) => {
                                if (result.value) {
                                    //save schedule uuid
                                    localStorage.setItem('scheduleUuid', uuid)
                                    //save move type
                                    localStorage.setItem('moveType', this.props.dataMoveType)
                                    localStorage.setItem('moveTypeLabel', this.props.dataMoveTypeLabel)

                                    this.props.history.push("/clickargo_register")
                                }
                            })
                        }
                    }
                })

        }, 1200);
    }

    durationFormatter(cell) {
        return (
            cell + ' Days'
        )
    }

    actionFormatter(cell) {
        return (
            <div>
                <Button
                    type="button"
                    color="default"
                    onClick={() => this.submitHandler(cell)}>
                    <img src={window.location.origin + '/assets/images/Clickargo2.png'} /><span>Book Now</span></Button >
            </div>
        )
    }

    render() {

        console.log(this.state)

        const { redirect } = this.state;

        if (redirect) {
            return <Redirect to='/booking_request' />
        }

        const options = {
            // pageStartIndex: 0,
            sizePerPage: 10,
            hideSizePerPage: true,
            hidePageListOnlyOnePage: true
        };

        const columns = [
            {
                dataField: 'vessel_name',
                text: 'Vessel',
                sort: true
            },
            {
                dataField: 'carrier_name',
                text: 'Agent',
                sort: true
            },
            {
                dataField: 'service_name',
                text: 'Service',
                sort: true
            },
            {
                dataField: 'voyage_name',
                text: 'Voyage No.',
                sort: true
            },
            {
                dataField: 'imo_number',
                text: 'Imo',
                sort: true
            },
            {
                dataField: 'origin_departure_date',
                text: 'Departure',
                sort: true
            },
            {
                dataField: 'destination_arrival_date',
                text: 'Arrival',
                sort: true
            },
            {
                dataField: 'origin',
                text: 'Origin'
            },
            {
                dataField: 'destination',
                text: 'Destination'
            },
            {
                dataField: 'total_duration',
                text: 'Duration',
                sort: true,
                formatter: this.durationFormatter
            },
            {
                dataField: 'uuid',
                text: '',
                formatter: this.actionFormatter
            }
        ];

        if (this.props.dataSchedule === null) {
            return (
                this.state.isLoading ? (
                    <div>
                        <div>
                            <hr style={{ border: "10px solid #f3f3f7" }} />
                        </div>

                        <div className="kt-portlet__head">
                            <div className="kt-portlet__head-label">
                                <h3 className="kt-portlet__head-title">
                                    <span>
                                        <label className="kt-font-boldest"> Schedule List</label>
                                    </span>
                                </h3>
                            </div>
                        </div>

                        <div className="kt-portlet__body">
                            <div className="kt-widget15">
                                <BootstrapTable
                                    bootstrap4
                                    keyField='uuid'
                                    data={[]}
                                    columns={columns}
                                    pagination={paginationFactory(options)} />
                            </div>
                        </div>

                    </div>
                )
                    : (
                        <div>
                            <div>
                                <hr style={{ border: "10px solid #f3f3f7" }} />
                            </div>

                            <div className="kt-portlet__head">
                                <div className="kt-portlet__head-label">
                                    <h3 className="kt-portlet__head-title">
                                        <span>
                                            <label className="kt-font-boldest"> Schedule List</label>
                                        </span>
                                    </h3>
                                </div>
                            </div>

                            <div className="kt-portlet__body">
                                <div className="kt-widget15">
                                    <Spinner animation="grow" variant="primary" size="md" style={styles.spinnerStyle} />
                                </div>
                            </div>
                        </div>
                    )
            )
        } else {
            return (
                this.state.isLoading ? (
                    <div>
                        <div>
                            <hr style={{ border: "10px solid #f3f3f7" }} />
                        </div>

                        <div className="kt-portlet__head">
                            <div className="kt-portlet__head-label">
                                <h3 className="kt-portlet__head-title">
                                    <span>
                                        <label className="kt-font-boldest"> Schedule List</label>
                                    </span>
                                </h3>
                            </div>
                        </div>

                        <div className="kt-portlet__body">
                            <div className="kt-widget15">
                                <BootstrapTable
                                    bootstrap4
                                    keyField='uuid'
                                    data={this.props.dataSchedule}
                                    columns={columns}
                                    pagination={paginationFactory(options)} />
                            </div>
                        </div>
                    </div>
                )
                    : (
                        <div>
                            <div>
                                <hr style={{ border: "10px solid #f3f3f7" }} />
                            </div>

                            <div className="kt-portlet__head">
                                <div className="kt-portlet__head-label">
                                    <h3 className="kt-portlet__head-title">
                                        <span>
                                            <label className="kt-font-boldest"> Schedule List</label>
                                        </span>
                                    </h3>
                                </div>
                            </div>

                            <div className="kt-portlet__body">
                                <div className="kt-widget15">
                                    <Spinner animation="grow" variant="primary" size="md" style={styles.spinnerStyle} />
                                </div>
                            </div>
                        </div>
                    )
            )
        }
    }
}

const styles = {
    spinnerStyle: {
        // flex: 1,
        alignSelf: 'center'
    }
};

export default withRouter(Schedule)