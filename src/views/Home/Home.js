import React, { Component } from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import { NavLink } from 'react-router-dom';
import "../assets/css/Home.css"


export default class Home extends Component {
    constructor() {
        super()
        this.state = {
            showMobileSideMenu: false,
        }
    }

    handleCloseHeaderMobileToggler = (ev) => {
        this.setState({ showMobileSideMenu: false });
    }
    handleHeaderMobileToggler = (ev) => {
        this.setState({ showMobileSideMenu: true });
    }

    render() {
        return (
            <>
               
                <div className="kt-header-mobile__logo kt-header-fixed" style={{ backgroundColor: "#051a92" }}>
                    <img width="240px" height="60px" src={require('../assets/image/logo-icon-transparent.png')} alt="NLE"></img>
                </div>
            

                <div id="kt_header" className="kt-header kt-grid kt-grid--ver  kt-header--fixed ">

                    <button className="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn" onClick={(ev) => this.handleCloseHeaderMobileToggler(ev)}><i className="la la-close"></i></button>
                    <div className="kt-header__topbar">
                        <div className="kt-header__topbar-item kt-header__topbar-item--search">
                            <div className="kt-header__topbar-wrapper">
                                <div className="kt-quick-search" id="kt_quick_search_default">
                                    <form method="get" className="kt-quick-search__form">
                                        <div className="input-group">
                                            <div className="input-group-prepend"><span className="input-group-text"><i className="flaticon2-search-1" /></span></div>
                                            <input type="text" className="form-control kt-quick-search__input" placeholder="Search anything..." />
                                            <div className="input-group-append"><span className="input-group-text"><i className="la la-close kt-quick-search__close" /></span></div>
                                        </div>
                                    </form>
                                    <div id="kt_quick_search_toggle" data-toggle="dropdown" data-offset="10px, 0px" />
                                    <div className="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
                                        <div className="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height={300} data-mobile-height={200}>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="kt-header__topbar">


                        <div className="kt-header__topbar-item kt-header__topbar-item--user">


                            <Dropdown className="kt-header__topbar-wrapper">
                                <Dropdown.Toggle
                                    variant="success"
                                    id="dropdown-basic"
                                    className="btn btn-outline-brand btn-icon btn-circle"
                                    style={{ marginTop: "10px", backgroundColor: "white", borderColor: "" }}>
                                    <i style={{ color: "#051a92", width: "10px" }} className="flaticon-bell"></i>
                                </Dropdown.Toggle>

                                <Dropdown.Menu style={{ marginTop: "15px" }} className="kt-menu__nav ">

                                    <Dropdown.Item className="kt-nav__item" >Action</Dropdown.Item>
                                    <Dropdown.Item className="kt-nav__item">Another action</Dropdown.Item>
                                    <Dropdown.Item className="kt-nav__item">Something else</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>

                        <div className="kt-header__topbar-item kt-header__topbar-item--user">

                            <Dropdown className="kt-header__topbar-wrapper">
                                <Dropdown.Toggle
                                    variant="success"
                                    id="dropdown-basic"
                                    className="btn btn-outline-brand btn-icon btn-circle"
                                    style={{ marginTop: "10px", backgroundColor: "white", borderColor: "none" }}
                                >
                                    <i style={{ color: "#051a92", width: "10px" }} className="flaticon2-user-outline-symbol"></i>
                                </Dropdown.Toggle>

                                <Dropdown.Menu style={{ marginTop: "15px" }} className="kt-menu__nav ">

                                    <Dropdown.Item className="kt-nav__item" >My Profile</Dropdown.Item>
                                    <Dropdown.Item className="kt-nav__item">Account settings and more</Dropdown.Item>
                                    <Dropdown.Item className="kt-nav__item" >Sign Out</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                    </div>
                </div>


                <div className="padding">

                    <div className="back">

                        <div className="col-sm-12 col-md-6 col-lg-4">
                            <div className="services-grid">
                                <NavLink to="/inbound/dashboard">
                                <div className="service service1" style={{position: 'relative'}}>
                                    <img width="300px" height="500px" src={require("../assets/image/background-2.png")} alt="inbound"></img>
                                        <div className="text-block">
                                            <h2>Inbound</h2>
                                        </div>
                                    </div>    
                                </NavLink>  
                                <NavLink to="/outbound/dashboard">            
                                <div className="service service1" style={{position: 'relative'}}>
                                    <img width="300px" height="500px" src={require("../assets/image/background-1.png")} alt="outbound"></img>
                                    <div className="text-block">
                                            <h2>Outbond</h2>
                                    </div>
                                </div>    
                                </NavLink> 
                                <NavLink to="/domestic/dashboard">
                                <div className="service service1" style={{position: 'relative'}}>
                                    <img width="300px" height="500px" src={require("../assets/image/background.png")} alt="domestics"></img>
                                    <div className="text-block">
                                            <h2>Domestic</h2>
                                    </div>
                                </div>

                                </NavLink>
                               
                               
                            </div>
                        </div>


                    </div>

                </div>

            </>

        )
    }
}
