import React,{Component, Fragment} from 'react';
import * as Constants from '../../../Constants';
import * as Helper from '../../../Helper';
import axios from 'axios'
import { MDBDataTable } from 'mdbreact';

class Track extends Component{
  constructor(props){
    super(props)
    this.state = {
      loadingDataTable: true,
      loadingTrackingDocuments: true,
      loadingTrackingTrucks: true,
      documents: [],
      npwp: '018245894059000',
      activePage: 'datatable',
      activeDocuments: false,
      activeDocumentsStatus: [],
      activeTruckingStatus: [],
      activeTab: 'tracking',
      details: [{
        product: {
          hsCode: '0306.16.22',
          desc: 'Udang Vaname',
          alias: 'Liptopenaeus Vannamei',
          merk: 'BabaMirza',
          price: 'IDR 83.000',
          tax: '10%',
          unit: '200 Box',
          netto: '10000 KGM',
        },
        documents: [{
          type: 'Quarantine',
          num: '3212/KP/2019',
          date: '17 Aug 2019'
        }, {
          type: 'COO',
          num: '3212/KP/2019',
          date: '27 Aug 2019'
        }],
        fob: 'IDR 830.000.000'
      }, {
        product: {
          hsCode: '0301.94.00',
          desc: 'Ikan Tuna',
          alias: 'Thunnus Orientalis',
          merk: 'SamAmir',
          price: 'IDR 48.000',
          tax: '0%',
          unit: '500 Box',
          netto: '20000 KGM',
        },
        documents: [{
          type: 'Quarantine',
          num: '3212/KP/2019',
          date: '17 Aug 2019'
        }, {
          type: 'COO',
          num: '3212/KP/2019',
          date: '27 Aug 2019'
        }],
        fob: 'IDR 960.000.000'
      }, {
        product: {
          hsCode: '0302.89.18',
          desc: 'Ikan Kakap Merah',
          alias: 'Lutjanus Argentimaculatus',
          merk: 'BossPur',
          price: 'IDR 60.000',
          tax: '0%',
          unit: '300 Box',
          netto: '15000 KGM',
        },
        documents: [{
          type: 'Quarantine',
          num: '3212/KP/2019',
          date: '17 Aug 2019'
        }, {
          type: 'COO',
          num: '3212/KP/2019',
          date: '27 Aug 2019'
        }],
        fob: 'IDR 900.000.000'
      }, {
        product: {
          hsCode: '0306.33.00',
          desc: 'Kepiting',
          alias: 'Liocarcinus Depurator',
          merk: 'CakDeny',
          price: 'IDR 85.000',
          tax: '0%',
          unit: '100 Box',
          netto: '10000 KGM',
        },
        documents: [{
          type: 'Quarantine',
          num: '3212/KP/2019',
          date: '17 Aug 2019'
        }, {
          type: 'COO',
          num: '3212/KP/2019',
          date: '27 Aug 2019'
        }],
        fob: 'IDR 850.000.000'
      }]
    }
  }
  setTab(tabName) {
    this.setState({activeTab: tabName});
  }
  // get api
  componentDidMount(){
    let config = {
      method: 'GET',
      url: `${Constants.API_DOCUMENTS}${this.state.npwp}`,
      headers: { 'Content-Type': 'application/json' },
    }
    axios.request(config).then(response => this.setState({
      documents: response.data['Data Active Documents'],
      loadingDataTable: false
    })).catch(error => {
      console.log('error: ', error)
    })
  }
  // end of get api
  
  addClickEventtoDocuments(){
    return (
      this.state.documents.map((i, bl_number, bl_date) => ({
        STATUS: i.STATUS,
        TGL_BL: i.TGL_BL,
        NO_PIB: i.NO_PIB,
        PIB: i.PIB,
        NO_BL: i.NO_BL,
        TGL_PIB: i.TGL_PIB,
        OPTIONS: (<button className="btn-label-info btn btn-sm btn-bold btn-action" onClick={() => this.showDetail(bl_number, bl_date)}>Details</button>),
        clickEvent: () => this.showDetail(bl_number, bl_date)
      }))
    )
  }
  // end of add Click Event to Rows

  // event of detail on click
  showDetail(index){
    const data = this.state.documents[index];
    this.setState({
      activePage: 'tracking',
      activeDocuments: data,
      loadingTrackingDocuments: true,
      loadingTrackingTrucks: true
    }, () => {
      let docStatusLoading = true;
      let truckStatusLoading = true;

      //Fetching log document status
      let config = {
        method: 'GET',
        url: Constants.API_DOCUMENTS_LOG_STATUS + data.NO_BL + '/' + data.TGL_BL.replace(/-/g, ""),
        headers: { 'Content-Type': 'application/json' },
      }
      axios.request(config).then(response => {
        this.setState({
          activeDocumentsStatus: response.data['Status PIB'],
          loadingTrackingDocuments: false
        })
      }).catch(error => {
        this.setState({
          activeDocumentsStatus: [],
          loadingTrackingDocuments: false
        })
        console.log('error: ', error)
      });

      //Fetching data log trucking status
      let configTruckingStatus = {
        method: 'GET',
        url: Constants.API_TRUCKING_LOG_STATUS + '?npwp='+this.state.npwp+'&blNo=MCC172816',
        headers: { 'Content-Type': 'application/json' },
      }
      axios.request(configTruckingStatus).then(response => {
        this.setState({
          activeTruckingStatus: response.data.content,
          loadingTrackingTrucks: false
        })
      }).catch(error => {
        this.setState({
          activeTruckingStatus: [],
          loadingTrackingTrucks: false
        })
        console.log('error: ', error)
      });
    });
    //alert('bl_number: ' + this.state.documents[index].NO_BL + " bl_date: " + this.state.documents[index].TGL_BL)
  }
  showPage = (page) => {
    this.setState({
      activePage: page,
      activeDocuments: false
    });
  }
  // end of event detail on click
  contentCustomsHeader = (breadcrump) =>{
    return(
      <div className="kt-portlet__head">

        <div className="kt-portlet__head-label">
            <h3 className="kt-portlet__head-title">
            {
              breadcrump.map((bcp, i) => {
                return (
                  <span key={i}>
                  {
                    bcp.activePage === null 
                    ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                    : (<span className="breadcrump_link link" onClick={()=>this.showPage(bcp.activePage)} key={i}>{bcp.label}</span>)
                  } 
                  {
                    i !== (breadcrump.length-1)
                    ? " > "
                    : null
                  }
                  </span>
                )
              })
            }
            </h3>
        </div>    
      </div>
    )
  } 
  renderDatatable() {
    const {documents, loadingDataTable} = this.state;
    const breadcrump = [
      {
        label: 'Customs',
        link: null
      }
    ];
    const data = {
      columns: [
      {
        label: 'Document Type',
        field: 'PIB',
        sort: 'asc',
        width: 150
      },
      {
        label: 'PIB Number',
        field: 'NO_PIB',
        sort: 'asc',
        width: 270
      },
      {
        label: 'PIB Date',
        field: 'TGL_PIB',
        sort: 'asc',
        width: 270
      },
      {
        label: 'Status',
        field: 'STATUS',
        sort: 'asc',
        width: 270
      },
      {
        label: '',
        field: 'OPTIONS',
        width: 270
      },
      ],
      rows: this.addClickEventtoDocuments()
    }
    return(
      <div className="kt-portlet kt-portlet--height-fluid">
        {this.contentCustomsHeader(breadcrump)}
        <Fragment>
          <div className="kt-portlet__body">
            <div className="kt-widget15">
              <div className="table-responsive">
                {
                  loadingDataTable ? (
                    <div className='text-center'><div className='spinner-border d-inline-block' role='status'></div></div>
                  ): (
                    <MDBDataTable
                      className='text-center'
                      style={{ cursor: "pointer" }}
                      striped
                      hover
                      data={data}
                    />
                  )
                }
              </div>
            </div>
          </div>
        </Fragment>
      </div>
    );   
  }
  renderTrackingDocuments() {
    const {activeDocuments, activeTab, activeTruckingStatus, activeDocumentsStatus, loadingTrackingDocuments, loadingTrackingTrucks} = this.state;
    const breadcrump = [
      {
        label: 'Customs',
        activePage: 'datatable',
      },
      {
        label: 'Tracking',
        activePage: null
      }
    ];
    const trackingLoader = (
      <li>
        <span/>
        <div>
          <p className="kt-font-bold">Loading ...</p>
        </div>
      </li>
    );
    //Load log status document
    let contentTrackingDocuments = '';
    if ( loadingTrackingDocuments ) {
      contentTrackingDocuments = trackingLoader
    } else if ( activeDocumentsStatus.length ) {
      contentTrackingDocuments = activeDocumentsStatus.map((docStatus, index) => {
        return (
          <li key={index}>
            <span/>
            <div>
              <p className="kt-font-bold">{docStatus.STATUS}</p>
              <p className="kt-font-info">{docStatus.WK_REKAM}</p>
            </div>
          </li>
        )
      })
    }

    //Load log status trucking
    let contentTrackingTruck = '';
    if ( loadingTrackingTrucks ) {
      contentTrackingTruck = trackingLoader
    } else if ( activeTruckingStatus.length ) {
      contentTrackingTruck = activeTruckingStatus.map((truckStatus, index) => {
        return (
          <>
          <li key={index}>
            <span className="groupList"><img src="./assets/images/container-icon.png"/></span>
            <div>
              <p className="kt-font-bold">{truckStatus.container_no}</p>
            </div>
          </li>
          {
            truckStatus.logtruck.length ? truckStatus.logtruck.map((log, i) => {
              return (
                <li key={i}>
                  <span/>
                  <div>
                    <p className="kt-font-bold">{log.status}</p>
                    <p className="kt-font-info">{log.create_date}</p>
                  </div>
                </li>
              )
            }): null
          }
          </>
        )
      })
    }

    return(
      <div className="kt-portlet kt-portlet--height-fluid">
        {this.contentCustomsHeader(breadcrump)}
        <Fragment>
          <div className="kt-portlet__body">
            <div className="kt-widget15">
              <div className="row">
                <div className="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                  <strong>PIB</strong><br/>{activeDocuments.PIB}
                </div>
                <div className="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                  <strong>NO PIB</strong><br/>{activeDocuments.NO_PIB}
                </div>
                <div className="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                  <strong>TGL PIB</strong><br/>{Helper.formatPrintDate(activeDocuments.TGL_PIB)}
                </div>
                <div className="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                  <strong>STATUS</strong><br/>{activeDocuments.STATUS}
                </div>
                <div className="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                  <strong>NO BL</strong><br/>{activeDocuments.NO_BL}
                </div>
                <div className="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                  <strong>TGL BL</strong><br/>{Helper.formatPrintDate(activeDocuments.TGL_BL)}
                </div>
              </div>
            </div>
            <div className="kt-widget15">
              <ul className="nav nav-tabs nav-tabs-line nav-tabs-line-danger nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
                <li className="nav-item">
                  <a className={'nav-link ' + (activeTab === 'tracking' ? 'active' : '')} data-toggle="tab" role="tab" onClick={() => this.setTab('tracking')}>
                    <i className="flaticon2-location" aria-hidden="true"></i>Tracking
                  </a>
                </li>
              </ul>
            </div>
            <div className="kt-widget15">
              {
                activeTab === 'tracking' ? (
                  <div className="tab-pane active p-4" role="tabpanel">
                    <div className="">
                      <p><b>BL Number : {activeDocuments.NO_BL}</b></p>
                      <br />
                      <ul className="list-ic horizontal">
                        <li>
                          <img className="img-fluid" src={'/media/ceisa/document-1.jpg'} alt={'Document 1'} />
                          <ul className="list-ic vertical">

                          </ul>
                        </li>
                        <li>
                          <img className="img-fluid" src={'/media/ceisa/document-2.jpg'} alt={'Document 2'} />
                          <ul className="list-ic vertical">
                            {contentTrackingDocuments}
                          </ul>
                        </li>
                        <li>
                          <img className="img-fluid" src={'/media/ceisa/document-3.jpg'} alt={'Document 3'} />
                          <ul className="list-ic vertical">
                            {contentTrackingTruck}
                          </ul>
                        </li>
                        <li className="active">
                          <img className="img-fluid" src={'/media/ceisa/document-4.jpg'} alt={'Document 4'} />
                          <ul className="list-ic vertical">
                            
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                ) : null
              }
              <a className="link" onClick={() => this.showPage('datatable')}><span className="fa fa-chevron-left"></span> Back to Datatable</a>
            </div>
          </div>
        </Fragment>
      </div>
    ); 
  }
  render(){
    const {activePage} = this.state;
    if ( activePage === "tracking" ) {
      return this.renderTrackingDocuments();
    } else {
      return this.renderDatatable();
    }
  }
}

export default Track;