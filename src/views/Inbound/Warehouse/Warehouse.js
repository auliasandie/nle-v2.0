import React,{Component, Fragment} from 'react';
import {NavLink, HashRouter} from 'react-router-dom';
import {Map,GoogleApiWrapper,Marker} from 'google-maps-react';
import * as Constants from '../../../Constants';
import * as Helper from '../../../Helper';
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import Highcharts from "highcharts/highstock";
import HighchartsReact from "highcharts-react-official";

class Warehouse extends Component{
  constructor(props){
    super(props)
    this.state = {
      markers: [
        {
          icon: 'assets/images/warehouse-icon-marker.png',
          lat: '-6.1023223',
          lng: '106.9019725'
        },
        {
          icon: 'assets/images/warehouse-icon-marker.png',
          lat: '-6.307923199999999',
          lng: '107.17208499999992'
        },
        {
          icon: 'assets/images/warehouse-icon-marker.png',
          lat: '-6.1404077',
          lng: '106.9370861'
        }
      ]
    }
  }

  contentWarehouseHeader = (breadcrump) =>{
    return(
      <div className="kt-portlet__head">

        <div className="kt-portlet__head-label">
            <h3 className="kt-portlet__head-title">
            {
              breadcrump.map((bcp, i) => {
                return (
                  <span key={i}>
                  {
                    bcp.link === null 
                    ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                    : (<a href="/#/warehouse/" className="kt-link kt-font-transform-u" onClick={()=>{this.setActiveView(bcp.link)}} key={i}>{bcp.label}</a>)
                  } 
                  {
                    i !== (breadcrump.length-1)
                    ? " > "
                    : null
                  }
                  </span>
                )
              })
            }
            </h3>
        </div>    
      </div>
    )
  } 
  contentMapsWareHouse = () =>{
    const {markers} = this.state;
    return(
      <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid" style={{borderBottom:"2px solid #99999982", borderRight:"2px solid #99999982"}}>
        <div className="kt-portlet__body">
          <div className="kt-widget15">
            <div className="kt-widget15__map">
              <div style={{height: "450px", position: "relative", overflow: "hidden"}}>
                <div style={{height: "100%", width: "100%", position: "absolute",
                    top: "0px", left: "0px"}}>
                  <div className="gm-err-container">
                    <Map
                      google={this.props.google} 
                      zoom={13} 
                      initialCenter={{lat: '-6.1066044', lng: '106.8942988'}}
                      bounds={new this.props.google.maps.LatLngBounds()}
                    >
                    {
                      markers.map((store, index) => {
                        return <Marker key={index} id={index}
                          icon={store.icon ? store.icon: null}
                          position={{lat: parseFloat(store.lat),lng: parseFloat(store.lng)}}
                          />
                      })
                    }
                    </Map>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  render(){
    const breadcrump = [
      {
        label: 'Warehouse',
        link: null
      }
    ];
    return(
      <Fragment>
        <div className="kt-portlet kt-portlet--height-fluid">
          {this.contentWarehouseHeader(breadcrump)}
        </div>
          {this.contentMapsWareHouse()}
      </Fragment>
    )
  }

}

export default GoogleApiWrapper({
  apiKey: (Constants.googleMapAPI)
})(Warehouse);