import React, { Component } from 'react';
import { Tab, Tabs, Form, Col, Button } from 'react-bootstrap';
// import { Link } from 'react-router-dom'
import DO from '../DO/DO';
import Customs from '../Customs/Customs'
import SP2 from '../SP2/SP2';
import Select from 'react-select';
import Trucking from '../Trucking/Trucking';
import Track from '../Track and Trace/Track';
import Warehouse from '../Warehouse/Warehouse';

export default class CargoOwner extends Component {
    constructor() {
        super()
        this.state = {
            delivery: {},
            customs: {},
            sp2: {},
            key: 1
        }
        this.handleSelect = this.handleSelect.bind(this)
    }

    showData(data) {
        console.log(data)
        this.setState({
            key: data
        })
    }

    handleSelect(key) {
        this.setState({
            key
        })
    }
    render() {
        return (
            <>
                <div className="row" style={{ marginBottom: "10px" }}>
                    <h5 style={{ display: "inline", marginRight: '10px' }}>Documents</h5>
                    <div style={{marginLeft: '8px'}}>
                        <Form>
                            <Form.Row className="align-items-center">
                                <Col sm="6">
                                    <Form.Control size="sm" as="select" >
                                        <option>Document Type</option>
                                    </Form.Control>
                                </Col>
                                <Col sm="6">
                                    <Form.Control size="sm" type="text" placeholder="Nomor Aju" />
                                </Col>
                            </Form.Row>
                        </Form>
                    </div>
                   
                    
                </div>
                <div className="row" style={{ marginBottom: "10px" }}>
                <h5 style={{ display: "inline", marginRight: '13px' }}>Bill of Lading</h5>
                    <div>
                        <Form>
                            <Form.Row className="align-items-center">
                                <Col sm="4">
                                <Form.Control size="sm" type="text" placeholder="No. Bill of Lading" />
                                </Col>
                                <Col sm="4">
                                    <Form.Control size="sm" type="text" placeholder="Tgl Bill of Lading" />
                                </Col>
                                <Col xs="auto">
                                    <Button className="mb-2" size="sm" style={{ marginRight: '10px' }}>
                                        Add
                                                             </Button>
                                    <Button variant="dark" className="mb-2" size="sm">
                                        Search
                                                             </Button>
                                </Col>
                            </Form.Row>
                        </Form>
                    </div>
                </div>
                <hr></hr>
                <div className="kt-portlet kt-portlet--height-fluid">
                    <table className="table table-sm" style={{ textAlign: 'left' }}>
                        <thead>
                            <tr>
                                <th scope="col" style={{textAlign:"left"}}>No</th>
                                <th scope="col" style={{textAlign:"left"}}>Document Type</th>
                                <th scope="col" style={{textAlign:"left"}}>No. Document</th>
                                <th scope="col" style={{textAlign:"left"}}>Bill of Lading</th>
                                <th scope="col" style={{textAlign:"left"}}>Delivery Order</th>
                                <th scope="col" style={{textAlign:"left"}}>Customs</th>
                                <th scope="col"style={{textAlign:"left"}} >SP2</th>
                                <th scope="col" style={{textAlign:"left"}}>Trucking</th>
                                <th scope="col" style={{textAlign:"left"}}>Depo</th>
                                <th scope="col" style={{textAlign:"left"}}>Warehouse</th>
                                <th scope="col" style={{textAlign:"left"}}>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Impor</td>
                                <td>75894</td>
                                <td>75894</td>
                                <td onClick={() => this.showData(1)}><img src={require('../../assets/image/correct.png')}></img></td>
                                <td onClick={() => this.showData(2)}><img src={require('../../assets/image/correct.png')}></img></td>
                                <td onClick={() => this.showData(3)}><img src={require('../../assets/image/correct.png')}></img></td>
                                <td onClick={() => this.showData(4)}>Booking Order</td>
                                <td onClick={() => this.showData(5)}>Proses</td>
                                <td onClick={() => this.showData(6)}>Proses</td>
                                <td onClick={() => this.showData(7)} style={{color: 'blue'}}>View</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>PLB</td>
                                <td>756483</td>
                                <td>756483</td>
                                <td onClick={() => this.showData(1)}>BILL</td>
                                <td  onClick={() => this.showData(2)}>Penjaluran</td>
                                <td onClick={() => this.showData(3)}>PAID</td>
                                <td onClick={() => this.showData(4)}>Pembayaran</td>
                                <td onClick={() => this.showData(5)}>Proses</td>
                                <td onClick={() => this.showData(6)}>Proses</td>
                                <td onClick={() => this.showData(7)} style={{color: 'blue'}}>View</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>PLB</td>
                                <td>7353725</td>
                                <td>7353725</td>
                                <td onClick={() => this.showData(1)}>BILL</td>
                                <td  onClick={() => this.showData(2)}>Pemeriksaan</td>
                                <td onClick={() => this.showData(3)}>BILL</td>
                                <td onClick={() => this.showData(4)}>Menunggu Konfirmasi</td>
                                <td onClick={() => this.showData(5)}>Proses</td>
                                <td onClick={() => this.showData(6)}>Proses</td>
                                <td onClick={() => this.showData(7)} style={{color: 'blue'}}>View</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>TPB</td>
                                <td>832538</td>
                                <td>832538</td>
                                <td onClick={() => this.showData(1)}>PAID</td>
                                <td  onClick={() => this.showData(2)}>Pemeriksaan</td>
                                <td onClick={() => this.showData(3)}>PAID</td>
                                <td onClick={() => this.showData(4)}>Menunggu Konfirmasi</td>
                                <td onClick={() => this.showData(5)}>Proses</td>
                                <td onClick={() => this.showData(6)}>Proses</td>
                                <td onClick={() => this.showData(7)} style={{color: 'blue'}}>View</td>
                            </tr>
                            <tr >
                                <td>5</td>
                                <td >Impor</td>
                                <td>7352725</td>
                                <td>7352725</td>
                                <td onClick={() => this.showData(1)}>PAID</td>
                                <td  onClick={() => this.showData(2)}>Pemeriksaan</td>
                                <td onClick={() => this.showData(3)}>PAID</td>
                                <td onClick={() => this.showData(4)}>Menunggu Konfirmasi</td>
                                <td onClick={() => this.showData(5)}>Proses</td>
                                <td onClick={() => this.showData(6)}>Proses</td>
                                <td onClick={() => this.showData(7)} style={{color: 'blue'}}>View</td>
                            </tr>   
                        </tbody>
                    </table>
                </div>
                <hr></hr>
                <br></br>
               
                <div>
                    <Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="uncontrolled-tab-example">
                        <Tab eventKey={1} title="Delivery Order">
                            <DO />
                        </Tab>
                        <Tab eventKey={2} title="Customs">
                            <Customs />
                        </Tab>
                        <Tab eventKey={3} title="SP2">
                            <SP2 />
                        </Tab>  
                       <Tab eventKey={4} title="Trucking">
                            <Trucking />
                        </Tab>        
                        <Tab eventKey={5} title="Depo">
                            
                        </Tab>
                        <Tab eventKey={6} title="Warehouse">
                            <Warehouse/>
                        </Tab>
                        <Tab eventKey={7} title="Track and Trace">
                            <Track />
                        </Tab>
                    </Tabs>
                </div>
            </>
        )
    }
}