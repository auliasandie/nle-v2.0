import React, { Component } from 'react';

export default class Customs extends Component {
    render() {
        return (
            <>
                <table className="table table-sm" style={{ textAlign: 'center' }}>
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">No.Documents</th>
                            <th scope="col">Date Doc</th>
                            <th scope="col">Status</th>
                            <th scope="col">Date</th>
                            <th scope="col">Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>REQ12334455</td>
                            <td>REQ12334455</td>
                            <td>REQ12334455</td>
                            <td>BL123456780</td>
                            <td><button type="button" className="btn btn-primary btn-sm">View</button></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>REQ12334455</td>
                            <td>REQ12334455</td>
                            <td>REQ12334455</td>
                            <td>BL123456780</td>
                            <td><button type="button" className="btn btn-primary btn-sm">View</button></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>REQ12334455</td>
                            <td>REQ12334455</td>
                            <td>REQ12334455</td>
                            <td>BL123456780</td>
                            <td><button type="button" className="btn btn-primary btn-sm">View</button></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>REQ12334455</td>
                            <td>REQ12334455</td>
                            <td>REQ12334455</td>
                            <td>BL123456780</td>
                            <td><button type="button" className="btn btn-primary btn-sm">View</button></td>
                        </tr>
                    </tbody>
                </table>

            </>
        )
    }
}